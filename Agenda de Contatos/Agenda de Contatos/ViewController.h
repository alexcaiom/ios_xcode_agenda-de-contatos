//
//  ViewController.h
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 03/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ServicoUtils.h"

@interface ViewController : UIViewController <UIPageViewControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) IBOutlet UILabel* lblId;
@property (nonatomic, strong) IBOutlet UILabel* lblNome;

@property (nonatomic, strong) IBOutlet UITextField* txtPesquisaNome;

@property (nonatomic, strong) ServicoUtils* servico;

- (IBAction)pesquisar :(id)sender;

@end

