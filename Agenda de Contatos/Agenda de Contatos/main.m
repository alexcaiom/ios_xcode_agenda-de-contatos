//
//  main.m
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 03/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
