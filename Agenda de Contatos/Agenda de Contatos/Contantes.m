//
//  Contantes.m
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 13/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import "Contantes.h"

@implementation Contantes

static Contantes* instancia = nil;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.URL_BASE = @"http://alexcaiom.com.br/api-autenticacao/rest/";
    }
    return self;
}

+ (Contantes *)getInstancia {
    if (!instancia) {
        instancia = [Contantes init];
    }
    return instancia;
}

@end
