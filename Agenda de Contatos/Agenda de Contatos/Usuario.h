//
//  Usuario.h
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 08/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

@interface Usuario : Pessoa

@property (strong) NSString* login;
@property (strong) NSString* senha;
@property (strong) NSCalendar* dataCriacao;
@property (strong) NSString* status;

@end
