//
//  ServicoUtils.h
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 11/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServicoUtils : NSObject

@property (strong) NSString* URL_BASE;

@end
