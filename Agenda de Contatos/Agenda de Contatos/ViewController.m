//
//  ViewController.m
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 03/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.servico = [ServicoUtils init];
    [self pesquisar:nil];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pesquisar:(id)sender {
    NSString* dadosDePesquisa = [self getDadosDePesquisa];
    NSString* endereco = [NSString stringWithFormat:@"http://alexcaiom.com.br/api-autenticacao/rest/usuario%@", dadosDePesquisa];
    NSURL* url = [NSURL URLWithString:endereco];
    NSURLRequest* requisicao = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:requisicao queue:[NSOperationQueue mainQueue] completionHandler:
        ^(NSURLResponse* resposta, NSData* dados, NSError* erro){
            if (dados.length > 0 && erro == nil) {
                NSDictionary* jsonResposta = [NSJSONSerialization JSONObjectWithData:dados options:0 error:NULL];
                NSArray* lista = jsonResposta;
                
                if ([lista count] > 0) {
                    NSDictionary* objJSON = lista[0];
                    self.lblId.text = [[objJSON objectForKey:@"id"] stringValue];
                    self.lblNome.text = [objJSON objectForKey:@"nome"];
                }
                
//                self.lblId.text = []  [[corpoJSON objectForKey:@"id"] stringValue];
//                self.lblNome.text = [[corpoJSON objectForKey:@"nome"] stringValue];
            }
        }
     ];
}

- (NSString*) getDadosDePesquisa {
    NSString* nomePesquisado = self.txtPesquisaNome.text;
    if (nomePesquisado.length > 0) {
        return [NSString stringWithFormat:@"/pesquisarPorNome/%@", nomePesquisado];
    }
    return @"";
}


@end
