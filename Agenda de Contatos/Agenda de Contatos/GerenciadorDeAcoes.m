//
//  GerenciadorDeAcoes.m
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 18/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import "GerenciadorDeAcoes.h"


@implementation GerenciadorDeAcoes

- (id) initWithUsuario:(Usuario *)usuario {
    self = [super init];
    if (self) {
        self.usuario = usuario;
    }
    return self;
}

- (void) acoesDoController:(UIViewController *)controller {
    self.controller = controller;
    
    UIActionSheet* opcoes = [[UIActionSheet alloc] initWithTitle:self.usuario.nome delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:@"Ligar", @"Enviar e-mail", @"Site", @"Mapa", nil];
    
    [opcoes showInView:controller.view];
}

@end
