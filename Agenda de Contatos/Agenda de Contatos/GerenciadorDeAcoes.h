//
//  GerenciadorDeAcoes.h
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 18/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Usuario.h"
#import <MessageUI/MFMailComposeViewController.h>


@interface GerenciadorDeAcoes : NSObject <UIActionSheetDelegate, MFMailComposeViewControllerDelegate>

@property Usuario* usuario;
@property UIViewController* controller;
- (id) initWithUsuario : (Usuario*) usuario;
- (void) acoesDoController : (UIViewController*) controller;

@end
