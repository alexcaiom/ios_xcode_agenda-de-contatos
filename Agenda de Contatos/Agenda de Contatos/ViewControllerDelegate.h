//
//  ViewControllerDelegate.h
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 13/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Usuario.h"

@protocol ViewControllerDelegate <NSObject>

- (void) contatoAdd : (Usuario*) usuario;
- (void) contatoAtualizado : (Usuario*) usuario;

@end
