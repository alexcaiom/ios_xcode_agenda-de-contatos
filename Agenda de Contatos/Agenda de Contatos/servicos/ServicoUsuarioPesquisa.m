//
//  ServicoUsuarioPesquisa.m
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 09/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import "ServicoUsuarioPesquisa.h"

@implementation ServicoUsuarioPesquisa

- (instancetype)init {
    self = [super init];
    if (self) {
        self.constantes = [Contantes getInstancia];
    }
    return self;
}

- (NSMutableArray *)listar {
    return [self getDadosDePesquisaPorNome:@""];
}

- (NSMutableArray *)pesquisarPorNome:(NSString*)nome {
    NSMutableArray* listaResultados = [NSMutableArray new];
    
    
    NSString* dadosDePesquisa = [self getDadosDePesquisaPorNome:nome];
    NSString* endereco = [NSString stringWithFormat:@"%@usuario%@", self.constantes.URL_BASE, dadosDePesquisa];
    NSURL* url = [NSURL URLWithString:endereco];
    NSURLRequest* requisicao = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:requisicao queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse* resposta, NSData* dados, NSError* erro){
         if (dados.length > 0 && erro == nil) {
             NSDictionary* jsonResposta = [NSJSONSerialization JSONObjectWithData:dados options:0 error:NULL];
             NSArray* lista = jsonResposta;
             
             for (int i=0; i < lista.count; i++) {
                 NSDictionary* objJSON = lista[i];
                 Usuario* usuario = [Usuario new];
                 usuario.id = [objJSON objectForKey:@"id"];
                 usuario.nome = [objJSON objectForKey:@"nome"];
                 [listaResultados addObject:usuario];
             }
             
             
             
//             if ([lista count] > 0) {
             
//                 self.lblId.text = [[objJSON objectForKey:@"id"] stringValue];
//                 self.lblNome.text = [objJSON objectForKey:@"nome"];
//             }
             
             //                self.lblId.text = []  [[corpoJSON objectForKey:@"id"] stringValue];
             //                self.lblNome.text = [[corpoJSON objectForKey:@"nome"] stringValue];
        }
     }];
    
    return listaResultados;
}

- (NSString*) getDadosDePesquisaPorNome : (NSString*) nome {
    NSString* nomePesquisado = nome;
    if (nomePesquisado.length > 0) {
        return [NSString stringWithFormat:@"/pesquisarPorNome/%@", nomePesquisado];
    }
    return @"";
}

- (Usuario *)pesquisar:(NSInteger *)id {
    self.usuario = nil;
    NSString* endereco = [NSString stringWithFormat:@"%@usuario%d", self.constantes.URL_BASE, id];
    NSURL* url = [NSURL URLWithString:endereco];
    NSURLRequest* requisicao = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:requisicao queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse* resposta, NSData* dados, NSError* erro){
        if (dados.length > 0 && erro == nil) {
            NSDictionary* jsonResposta = [NSJSONSerialization JSONObjectWithData:dados options:0 error:NULL];
            NSDictionary* objUsuario = jsonResposta;
            
            self.usuario = [Usuario new];
            self.usuario.id = [objUsuario objectForKey:@"id"];
            self.usuario.nome = [objUsuario objectForKey:@"nome"];
        }
    }];
    
    
    return self.usuario;
}

@end
