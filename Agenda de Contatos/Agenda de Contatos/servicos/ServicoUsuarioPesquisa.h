//
//  ServicoUsuarioPesquisa.h
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 09/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Usuario.h"
#import "Contantes.h"

@interface ServicoUsuarioPesquisa : NSObject

- (NSMutableArray*) listar;
- (NSMutableArray*) pesquisarPorNome : (NSString*) nome;
- (Usuario*) pesquisar : (NSInteger*) id;

@property (strong) NSString* entidade;
@property (strong) Contantes* constantes;
@property (strong) Usuario* usuario;

@end
