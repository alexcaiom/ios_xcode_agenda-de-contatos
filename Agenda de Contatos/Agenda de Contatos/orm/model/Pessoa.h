//
//  Pessoa.h
//  Agenda de Contatos
//
//  Created by Alexander Massuda on 09/06/17.
//  Copyright © 2017 Alexander Massuda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong) NSNumber* id;
@property (strong) NSString* nome;
@property (strong) NSCalendar* dataNascimento;
@property (strong) NSString* email;
@property (strong) NSString* identificacao;
@property (strong) NSString* tipo;

@end
